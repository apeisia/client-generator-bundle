<?php

namespace Apeisia\ClientGeneratorBundle\DependencyInjection;

use Apeisia\ClientGeneratorBundle\EventSubscriber\ClassChangedSubscriber;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ApeisiaClientGeneratorExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $outputs = $config['outputs'];

        if ($outputs !== null) {

            $container->getDefinition(ClassChangedSubscriber::class)
                ->setArgument('$outputs', $config['outputs'])
            ;

        }
    }
}
