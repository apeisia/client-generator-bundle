<?php

namespace Apeisia\ClientGeneratorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('client_generator');
        $rootNode    = $treeBuilder->getRootNode();

        // @formatter:off
        $rootNode
            ->children()
                ->arrayNode('outputs')
                    ->arrayPrototype()
                        ->children()
                            ->enumNode('language')
                                ->values(['typescript', 'dart'])
                            ->end()
                            ->enumNode('type')
                                ->values(['models', 'api'])
                            ->end()
                            ->scalarNode('directory')->end()
                            ->scalarNode('importPrefix')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        ;
        // @formatter:on

        return $treeBuilder;
    }
}
