<?php

namespace Apeisia\ClientGeneratorBundle\Enum;

enum TypescriptEnumType: string
{
    /**
     * Generate a typescript union type ("type1" | "type2" | "type3")
     */
    case Union = 'Union';

    /**
     * Generate a native typescript enum
     */
    case Native = 'Native';
}
