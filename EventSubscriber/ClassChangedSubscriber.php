<?php

namespace Apeisia\ClientGeneratorBundle\EventSubscriber;

use Apeisia\ClientGeneratorBundle\CodeReader\ControllerReader;
use Apeisia\ClientGeneratorBundle\CodeReader\EnumReader;
use Apeisia\ClientGeneratorBundle\CodeReader\ModelReader;
use Apeisia\ClientGeneratorBundle\Model\GeneratedCode;
use Apeisia\ClientGeneratorBundle\Output\DartOutput;
use Apeisia\ClientGeneratorBundle\Output\TypescriptOutput;
use Apeisia\WatchBundle\Event\ClassChangedEvent;
use Apeisia\WatchBundle\Event\InitialScanCompletedEvent;
use Apeisia\WatchBundle\Event\TransformationFinishedEvent;
use Illuminate\Support\Str;
use Roave\BetterReflection\Reflection\ReflectionEnum;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ClassChangedSubscriber implements EventSubscriberInterface
{
    private EventDispatcherInterface $eventDispatcher;
    private ModelReader $exportedModelReader;
    private array $configuredOutputs;
    private ControllerReader $controllerReader;
    private EnumReader $enumReader;
    private array $outputWriters;
    private bool $hasModelOutputs;
    private bool $hasApiOutputs;
    private bool $isInitialScan = true;
    private array $filesToFormat = [];

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        ModelReader              $exportedModelReader,
        ControllerReader         $controllerReader,
        EnumReader               $enumReader,
        array                    $outputs,
        TypescriptOutput         $typescriptOutput,
        DartOutput               $dartOutput
    ) {
        $this->eventDispatcher     = $eventDispatcher;
        $this->exportedModelReader = $exportedModelReader;
        $this->controllerReader    = $controllerReader;
        $this->enumReader          = $enumReader;
        $this->configuredOutputs   = $outputs;
        $this->outputWriters       = [
            $typescriptOutput->getName() => $typescriptOutput,
            $dartOutput->getName()       => $dartOutput,
        ];
        $this->hasModelOutputs     = count(filter($outputs, fn($out) => $out['type'] == 'models')) > 0;
        $this->hasApiOutputs       = count(filter($outputs, fn($out) => $out['type'] == 'api')) > 0;

        foreach ($outputs as $output) {
            $generatedCodes = [];
            if ($output['type'] == 'models') {
                $generatedCodes = $this->outputWriters[$output['language']]->generateModelSupportingCode();
            } else if ($output['type'] == 'api') {
                $generatedCodes = $this->outputWriters[$output['language']]->generateClientSupportingCode();
            }
            foreach ($generatedCodes as $generatedCode) {
                $this->writeGeneratedCode($output, $generatedCode);
            }
        }
        $eventDispatcher->addListener(InitialScanCompletedEvent::class, function () {
            $this->isInitialScan = false;
            $this->formatFiles();
        });
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ClassChangedEvent::class => 'onClassChanged',
        ];
    }

    public function onClassChanged(
        ClassChangedEvent $event
    ) {

        $reflectionClass = $event->getBetterReflectionClass();
        if (
            $this->hasModelOutputs &&
            (
                str_contains($event->getClassName(), 'Entity') ||
                str_contains($event->getClassName(), 'Model') // this filter is also defined in NameConverter::convertClassName
            ) &&
            !str_contains($event->getClassName(), 'AccessorTrait') &&
            !str_contains($reflectionClass->getFileName(), '/vendor/')
        ) {
            echo $event->getClassName() . "\n";
            if ($reflectionClass instanceof ReflectionEnum) {
                $enumInfo = $this->enumReader->read($reflectionClass);
                foreach ($this->configuredOutputs as $output) {
                    if ($output['type'] == 'models') {
                        $this->writeGeneratedCode($output, $this->outputWriters[$output['language']]->generateEnum($enumInfo), $output['language'] != 'typescript');
                    }
                }
            } else {

                $modelInfo = $this->exportedModelReader->read($event->getBetterReflectionClass());
                if ($modelInfo->hasExportedProperties() || $modelInfo->getParent()) {
                    foreach ($this->configuredOutputs as $output) {
                        if ($output['type'] == 'models') {
                            $this->writeGeneratedCode($output, $this->outputWriters[$output['language']]->generateModel($modelInfo), $output['language'] != 'typescript');
                        }
                    }
                }
            }
        }

        if (
            $this->hasApiOutputs &&
            str_contains($event->getClassName(), 'Controller')
        ) {
            echo $event->getClassName() . "\n";
            $controllerInfo = $this->controllerReader->read($event->getBetterReflectionClass());
            if ($controllerInfo->hasEndpoints()) {
                foreach ($this->configuredOutputs as $output) {
                    if ($output['type'] == 'api') {
                        $this->writeGeneratedCode($output, $this->outputWriters[$output['language']]->generateClient($controllerInfo, $output['importPrefix']));
                    }
                }
            }
        }
    }

    private function writeGeneratedCode(array $configuredOutput, GeneratedCode $generatedCode, bool $format = true)
    {
        $this->eventDispatcher->dispatch(new TransformationFinishedEvent(
            self::class,
            $configuredOutput['directory'] . '/' . $generatedCode->getFilename(),
            $generatedCode->getCode()
        ));
        if($format) {
            $this->filesToFormat[] = $configuredOutput['directory'] . '/' . $generatedCode->getFilename();
        }
        if (!$this->isInitialScan) {
            $this->formatFiles();
        }
    }

    private function formatFiles()
    {
        if ($this->filesToFormat) {
            $dartFilesToFormat = filter($this->filesToFormat, fn($file) => str_ends_with($file, '.dart'));
            if ($dartFilesToFormat) {
                system("dart format -l 120 " . join(' ', map($dartFilesToFormat, fn($file) => escapeshellarg($file))));
            }
            $tsFilesToFormat = filter($this->filesToFormat, fn($file) => str_ends_with($file, '.ts'));
            if ($tsFilesToFormat) {
                system("prettier -w " . join(' ', map($tsFilesToFormat, fn($file) => escapeshellarg($file))));
            }
            $this->filesToFormat = [];
        }
    }
}
