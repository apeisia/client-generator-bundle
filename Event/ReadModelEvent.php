<?php

namespace Apeisia\ClientGeneratorBundle\Event;

use Apeisia\ClientGeneratorBundle\CodeReader\ModelReader;
use Apeisia\ClientGeneratorBundle\Model\ExportedProperty;
use Roave\BetterReflection\Reflection\ReflectionClass;

class ReadModelEvent
{
    private array $exportedProperties = [];

    public function __construct(
        public readonly ModelReader     $modelReader,
        public readonly ReflectionClass $class,
    )
    {
    }

    public function addExportedProperty(ExportedProperty $exportedProperty): void
    {
        $this->exportedProperties[] = $exportedProperty;
    }

    public function getExportedProperties(): array
    {
        return $this->exportedProperties;
    }
}
