<?php

namespace Apeisia\ClientGeneratorBundle\Output;

use Apeisia\AccessorTraitBundle\Util\CodeIndent;
use Apeisia\ClientGeneratorBundle\Annotation\GenerateClient;
use Apeisia\ClientGeneratorBundle\Annotation\TypescriptImport;
use Apeisia\ClientGeneratorBundle\Annotation\TypescriptRequired;
use Apeisia\ClientGeneratorBundle\Annotation\TypescriptType;
use Apeisia\ClientGeneratorBundle\CodeReader\NameConverter;
use Apeisia\ClientGeneratorBundle\Enum\TypescriptEnumType;
use Apeisia\ClientGeneratorBundle\Model\ApiEndpoint;
use Apeisia\ClientGeneratorBundle\Model\ApiParameter;
use Apeisia\ClientGeneratorBundle\Model\ControllerInfo;
use Apeisia\ClientGeneratorBundle\Model\EnumInfo;
use Apeisia\ClientGeneratorBundle\Model\ExportedProperty;
use Apeisia\ClientGeneratorBundle\Model\ExportedType;
use Apeisia\ClientGeneratorBundle\Model\GeneratedCode;
use Apeisia\ClientGeneratorBundle\Model\ModelInfo;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use LogicException;
use Roave\BetterReflection\Reflection\ReflectionEnumCase;

class TypescriptOutput implements OutputInterface
{
    public function getName(): string
    {
        return 'typescript';
    }

    public function generateModelSupportingCode(): array
    {
        return [];
    }


    public function generateClientSupportingCode(): array
    {
        return [];
    }

    /**
     * Takes a property and returns an array of typescript types as strings.
     * Also adds the necessary imports to the imports array.
     */
    public function propertyTypeToTypescriptType(ExportedProperty $property, array &$imports): array
    {
        // allow TypescriptType attribute to override the type
        if ($tsTypeAttributes = $property->getAttribute(TypescriptType::class)) {
            return Arr::map($tsTypeAttributes, fn(TypescriptType $tsType) => $tsType->type);
        }

        $types = [];

        foreach ($property->getTypes() as $type) {
            $name = $type->getName();

            if (str_ends_with($name, 'DateTime') || str_ends_with($name, 'DateTimeImmutable')) {
                $types[] = 'string /* DateTime */';
            } else if ($type->isScalar()) {
                if ($name == 'array') {
                    $types[] = $this->mapScalar('any[]');
                    $types[] = $this->mapScalar('any');
                } else {
                    $types[] = $this->mapScalar($name);
                }
            } else {
                $imports[] = $name;
                $types[]   = NameConverter::getClassname($name);
            }
        }
        if (count($types) == 0) {
            $types[] = 'any';
        }

        return $types;
    }

    public function generateModel(ModelInfo $classInfo): GeneratedCode
    {
        /** @var string[] $requiredImports */
        $requiredImports = [];

        $code = 'export interface ' . $classInfo->getClassname();
        if ($classInfo->hasParent()) {
            $requiredImports[] = $classInfo->getClass()->getParentClass()->getName();
            $code              .= ' extends ' . basename($classInfo->getParent());
        }
        $code .= " {\n";

        $properties = [];
        $imports    = [];

        foreach ($classInfo->getProperties() as $property) {
            // add imports that come from attributes
            foreach ($property->getAttribute(TypescriptImport::class) ?? [] as $import) {
                $imports[] = $this->buildImport($classInfo, $import);
            }

            // get the type of the resulting property
            $types = $this->propertyTypeToTypescriptType($property, $requiredImports);

            $types = filter($types, function ($type) {
                return $type !== 'null';
            });

            // if the property is marked as required, don't add the optional modifier
            $required = $property->getAttribute(TypescriptRequired::class);
            $qmark    = $required ? '' : '?';

            $propertyCode = "    /**\n";
            $propertyCode .= "     * Groups: " . join(', ', $property->getGroups()) . "\n";
            $propertyCode .= "     */\n";
            $propertyCode .= '    ' . $property->getName() . $qmark . ": " . join(' | ', $types) . "\n";

            $properties[] = $propertyCode;
        }

        $code .= join(PHP_EOL, $properties);
        $code .= "}\n";

        // normalize imports to never include array notation
        $requiredImports = Arr::map($requiredImports, fn(string $className) => Str::replaceEnd('[]', '', $className));

        foreach (array_unique($requiredImports) as $className) {
            $import = $this->generateImportString($classInfo, $className);

            if (!$import) {
                continue;
            }

            $imports[] = $import;
        }

        // when there are imports, concatenate them and make sure there is a blank line between imports and code
        $importsCode = implode(PHP_EOL, $imports) . "\n\n";
        if (empty($imports)) {
            // ... otherwise, there should be no blank line at the beginning of the file
            $importsCode = '';
        }

        return new GeneratedCode(
            $importsCode . $code,
            Str::snake($classInfo->getNamespace(), '-') . '/' . Str::snake($classInfo->getClassname(), '-') . '.ts'
        );
    }

    private function buildImport(ModelInfo $classInfo, TypescriptImport $import): string
    {
        if ($import->model) {
            return $this->generateImportString($classInfo, $import->model);
        }

        if ($import->name && $import->alias) {
            return "import type { $import->name as $import->alias } from \"$import->file\"";
        }

        if ($import->name) {
            return "import type { $import->name } from \"$import->file\"";
        }

        return '';
    }

    private function generateImportString(ModelInfo $classInfo, string $className): ?string
    {
        // we don't care about array types in imports, so we can strip them immediately
        $className = Str::replaceEnd('[]', '', $className);

        // prevent import of the class that causes the import (don't need to import itself, as it is already in the file)
        if ($className == $classInfo->getClass()->getName()) {
            return null;
        }

        // the name of the model that should be imported
        $importSymbol = Str::afterLast(basename($className), '\\');

        // importPath = the path to the file where the type is declared
        $dir        = $this->classNameToSnakeCasePath(NameConverter::getNamespace($className));
        $importPath = '@/model/' . $dir . '/' . Str::snake($importSymbol, '-');

        return "import type { $importSymbol } from \"$importPath\"";
        //. "\n// ^ className: $className\n// ^ importPath: $importPath \n// ^ importSymbol: $importSymbol\n// ^ dir: $dir";
    }

    private function classNameToSnakeCasePath(?string $className): string
    {
        if (!$className) {
            return '';
        }

        $parts = explode('\\', $className);

        $parts = Arr::map($parts, fn($part) => Str::snake($part, '-'));

        return implode('/', $parts);
    }

    public function generateEnum(EnumInfo $classInfo): GeneratedCode
    {
        return match ($classInfo->getEnumType()) {
            TypescriptEnumType::Native => $this->generateNativeEnum($classInfo),
            TypescriptEnumType::Union => $this->generateUnionEnum($classInfo),
        };
    }

    private function generateNativeEnum(EnumInfo $classInfo): GeneratedCode
    {
        $code  = 'export enum ' . $classInfo->getClassname() . " {\n";
        $cases = map($classInfo->getCases(), function (ReflectionEnumCase $case) {
            try {
                return $case->getValue() . ' = "' . $case->getName() . '"';
            } catch (LogicException) {
                return $case->getName();
            }
        });
        $code  .= CodeIndent::indent(join(",\n", $cases));
        $code  .= "\n}\n";

        return new GeneratedCode(
            $code,
            Str::snake($classInfo->getNamespace(), '-') . '/' . Str::snake($classInfo->getClassname(), '-') . '.ts'
        );
    }

    private function generateUnionEnum(EnumInfo $classInfo): GeneratedCode
    {
        $cases = map($classInfo->getCases(), function (ReflectionEnumCase $case) {
            try {
                return $case->getValue();
            } catch (LogicException) {
            }

            return $case->getName();
        });
        $code  = 'export type ' . $classInfo->getClassname() . ' = "' . join('" | "', $cases) . '"';

        return new GeneratedCode(
            $code,
            Str::snake($classInfo->getNamespace(), '-') . '/' . Str::snake($classInfo->getClassname(), '-') . '.ts'
        );
    }

    private function mapScalar(string $type): string
    {
        $map = [
            'bool'   => 'boolean',
            'float'  => 'number',
            'int'    => 'number',
            'double' => 'number',
        ];

        if (array_key_exists($type, $map)) {
            return $map[$type];
        }

        return $type;
    }

    public function generateClient(ControllerInfo $controllerInfo, string $importPrefix): GeneratedCode
    {
        $requiredImports = [];
        $code            = "import {useHttpClient} from '@/vf'\n\nexport function use" . $controllerInfo->getClientClassname() . "() {\n\nreturn {\n";
        foreach ($controllerInfo->getEndpoints() as $endpoint) {
            $code .= $this->generateMethod($endpoint, $requiredImports);
        }
        $code .= "};\n}\n";

        return new GeneratedCode($code, $controllerInfo->getClientClassname() . '.ts');
    }

    private function replaceReservedMethodNames(string $methodName): string
    {
        $reserved = [
            'new' => 'create',
        ];
        if (array_key_exists($methodName, $reserved)) {
            return $reserved[$methodName];
        }

        return $methodName;
    }

    private function generateMethod(ApiEndpoint $endpoint, array &$requiredImports): string
    {
        $method                   = strtolower($endpoint->getHttpMethods()[0]);
        $methodName               = $method . ucfirst(
                $this->replaceReservedMethodNames($endpoint->getClientMethodName())
            );
        $isWritingRequestWithData = in_array($method, ['post', 'put']);
        $passThruBody             = $isWritingRequestWithData && !$endpoint->hasRequestParameters();

        $parameters   = $this->buildParameters($endpoint, $passThruBody);
        $parameters[] = '$query?: Record<string, string|number>';

//        if ($isWritingRequestWithData) {
//            $namedParameters[] = 'ProgressCallback? $onSendProgress';
//        }
//        if ($isWritingRequestWithData || $method == 'get') {
//            $namedParameters[] = 'ProgressCallback? $onReceiveProgress';
//        }


        if ($endpoint->getGenerateClient()->specialType == GenerateClient::FORM) {
            $returnTypeName        = 'FormResponse';
            $decoder               = 'FormResponse.fromResponse';
            $returnIsScalar        = false;
            $returnTemplate        = false;
            $passResponseToDecoder = true;
        } else {
            $resolvedReturnType = $this->resolveType($endpoint->getReturnTypes());
            [$decoderClass, $returnIsScalar, $returnIsArray, , , $returnTemplate] = $resolvedReturnType;
            $returnTypeName = $this->resolveReturnTypeName($resolvedReturnType, $requiredImports);

            if ($returnTemplate) {
                $decoderMethod = 'fromJson';
            } elseif ($returnIsArray) {
                $decoderMethod = 'listFromJsonDecoder';
            } else {
                $decoderMethod = 'fromJsonDecoder';
            }

            $decoder               = $decoderClass . '.' . $decoderMethod;
            $passResponseToDecoder = false;
        }

        if (!in_array($returnTypeName, ['any', 'void', 'Uint8List'])) {
            $returnTypeName .= '|null';
        }
        $code = $this->generateSignature($returnTypeName, $methodName, $parameters);
        $code .= " => {\n";
        $code .= $this->generateNormalizationBlock($endpoint, $requiredImports);
        if ($passThruBody) {
            //$code   .= $this->generateFormBlock($method);
            $method = 'post';
        }
        if ($endpoint->hasQueryParameters()) {
            $code .= "final queryParameters = Map<String, dynamic>.from(\$query ?? {});\n";
            foreach ($endpoint->getQueryParameters() as $queryParameter) {
                $name = $queryParameter->getName();
                if ($queryParameter->isOptional()) {
                    $code .= "if($name != null) ";
                }
                $code .= "queryParameters[\"$name\"] = $name;\n";
            }
        }
        $code .= "return new Promise<string>(r => r('TODO'))\n";
        $code .= "  },\n";

        return $code;
    }


    private function buildParameters(ApiEndpoint $endpoint, bool $passThruBody): array
    {
        $parameters = [];
        foreach ($endpoint->getAllParameters() as $parameter) {
            $parameterType = $parameter->getType()->isScalar() ? $this->mapScalar(
                $parameter->getType()->getName()
            ) : 'any';
            if (!$parameter->isOptional()) {
                $parameters[] = $parameter->getName() . ': ' . $parameterType;
            } else {
                $parameters[] = $parameter->getName() . ($parameterType != 'any' ? '?' : '') . ': ' . $parameterType;
            }
        }
        if ($passThruBody) {
            $parameters[] = 'body: any';
        }

        return $parameters;
    }


    private function generateSignature(
        string $returnTypeName,
        string $methodName,
        array  $parameters
    ): string
    {
        $code = "\n " . $methodName . ": (";
        if (count($parameters)) {
            $code .= "\n    " . join(",\n    ", $parameters);
        }
        $code .= "\n ):  Promise<" . $returnTypeName . ">";

        return $code;
    }


    private function generateNormalizationBlock(ApiEndpoint $endpoint, array &$requiredImports): string
    {
        $code = '';
        /** @var ApiParameter $parameter */
        foreach ([...$endpoint->getPathParameters(), ...$endpoint->getQueryParameters()] as $parameter) {
            if ($parameter->getType()->isScalar()) {
                continue;
            }
            $name = $parameter->getName();
            if ($name == 'body') { // reserved variable names for named parameters
                $name = $name . '_';
            }
            [$typeName, , , , $namespace] = $this->resolveType([$parameter->getType()]);
            $requiredImports[] = $namespace . '/' . $typeName;
            $code              .= "    if (id in $name) $name = $name.id;\n" .
                "    if (" . ($parameter->isOptional() ? "!!$name &&" : '') . " typeof($name) !== 'string') throw Error('$name must be String or $typeName');\n";
        }

        return $code;
    }

    private function resolveType(array $types): array
    {
        if (
            count($types) == 1 ||
            (count($types) == 2 && count(filter($types, fn(ExportedType $t) => $t->getName() == 'null')) == 1)
        ) {
            $exportedType = $types[0];
            if (count($types) == 2) {
                $exportedType = filter($types, fn(ExportedType $t) => $t->getName() != 'null')[0];
            }
            $name    = $exportedType->getName();
            $isArray = false;
            if (str_ends_with($name, '[]')) {
                $name    = substr($name, 0, -2);
                $isArray = true;
            }
            if ($exportedType->isScalar()) {
                $isNull = true;
                if ($name == 'array') {
                    $name    = 'any';
                    $isArray = true;
                    $isNull  = false;
                }
                if ($name == 'void') {
                    $isNull = false;
                }

                return [$this->mapScalar($name), true, $isArray, $isNull, null, null];
            }
            $class         = NameConverter::convertClassName($exportedType->getName());
            $typeClassname = $class['name'];
            if ($isArray) {
                $typeClassname = substr($typeClassname, 0, -2);
            }

            return [$typeClassname, false, $isArray, true, $class['namespace'], $class['template']];
        }

        // name, isScalar, isArray, isNull, namespace, template
        return ['any', true, false, false, null, null];
    }


    private function resolveReturnTypeName(array $resolvedReturnType, array &$requiredImports): string
    {
        [$returnTypeName, $returnIsScalar, $returnIsArray, , $namespace, $returnTemplate] = $resolvedReturnType;
        if (!$returnIsScalar) {
            $requiredImports[] = $namespace . '/' . $returnTypeName;
            if ($returnTemplate) {
                $requiredImports[] = $returnTemplate['namespace'] . '/' . $returnTemplate['name'];
                $returnTypeName    .= '<' . $returnTemplate['name'] . '>';
            }
        }
        if ($returnTypeName == 'any' && $returnIsArray) {
            $returnTypeName = 'JsonKeyValue';
        } elseif ($returnIsArray) {
            $returnTypeName = 'List<' . $returnTypeName . '>';
        } elseif ($returnTypeName == 'binary') {
            $returnTypeName = 'Uint8List';
        }

        return $returnTypeName;
    }


}
