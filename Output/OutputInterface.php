<?php

namespace Apeisia\ClientGeneratorBundle\Output;

use Apeisia\ClientGeneratorBundle\Model\ControllerInfo;
use Apeisia\ClientGeneratorBundle\Model\EnumInfo;
use Apeisia\ClientGeneratorBundle\Model\GeneratedCode;
use Apeisia\ClientGeneratorBundle\Model\ModelInfo;

interface OutputInterface
{
    public function getName(): string;

    public function generateModelSupportingCode(): array;

    public function generateModel(ModelInfo $classInfo): GeneratedCode;

    public function generateEnum(EnumInfo $classInfo): GeneratedCode;

    public function generateClientSupportingCode(): array;

    public function generateClient(ControllerInfo $controllerInfo, string $importPrefix): GeneratedCode;
}
