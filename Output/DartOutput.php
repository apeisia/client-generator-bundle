<?php

namespace Apeisia\ClientGeneratorBundle\Output;

use Apeisia\ClientGeneratorBundle\Annotation\GenerateClient;
use Apeisia\ClientGeneratorBundle\CodeReader\NameConverter;
use Apeisia\ClientGeneratorBundle\Model\ApiEndpoint;
use Apeisia\ClientGeneratorBundle\Model\ApiParameter;
use Apeisia\ClientGeneratorBundle\Model\ControllerInfo;
use Apeisia\ClientGeneratorBundle\Model\EnumInfo;
use Apeisia\ClientGeneratorBundle\Model\ExportedType;
use Apeisia\ClientGeneratorBundle\Model\GeneratedCode;
use Apeisia\ClientGeneratorBundle\Model\ModelInfo;
use Illuminate\Support\Str;

class DartOutput implements OutputInterface
{
    public function getName(): string
    {
        return 'dart';
    }

    public function generateModelSupportingCode(): array
    {
        return [
            new GeneratedCode(file_get_contents(__DIR__ . '/../Resources/dart/types.dart'), 'types.dart'),
            new GeneratedCode(file_get_contents(__DIR__ . '/../Resources/dart/crud_list.dart'), 'base/crud_list.dart'),
        ];
    }


    public function generateClientSupportingCode(): array
    {
        return [
            new GeneratedCode(file_get_contents(__DIR__ . '/../Resources/dart/api_form_data.dart'), 'api_form_data.dart'),
            new GeneratedCode(file_get_contents(__DIR__ . '/../Resources/dart/form_response.dart'), 'form_response.dart'),
        ];
    }

    public function generateModel(ModelInfo $classInfo): GeneratedCode
    {
        $requiredImports  = ['types'];
        $superInitializer = '';

        $code = "/// Generated model. do not edit.\nclass " . $classInfo->getClassname();
        if ($classInfo->hasParent()) {
            $requiredImports[] = $classInfo->getParent();
            $code              .= ' extends ' . basename($classInfo->getParent());
            $superInitializer  = (count($classInfo->getProperties()) ? ",\n        " : '') . "super.fromJson(json)";
        }
        $code .= " {";

        $initializers   = [];
        $toMapDirect    = [];
        $toMapRecursive = [];

        foreach ($classInfo->getProperties() as $property) {
            $code .= "\n  ///  Groups: " . join(', ', $property->getGroups()) . "\n";

            [$typeName, $isScalar, $isArray, $isNull, $namespace] = $this->resolveType($property->getTypes());
            $typeIsBuiltIn = false;
            if (in_array($typeName, ['DateTime', 'DateTimeImmutable'])) {
                $typeName      = 'DateTime';
                $typeIsBuiltIn = true;
            }
            if ($isScalar || $typeIsBuiltIn || $isArray) {
                $toMapDirect[$property->getName()] = [$isArray, $isNull, $typeName];
            } else {
                $toMapRecursive[] = $property->getName();
            }

            $singleInitializer = fn($variable) => $typeName . '.fromJson(' . $variable . ')';
            $outType           = $typeName . ($isNull && $typeName != 'dynamic' ? '?' : '');
            if ($typeName == 'DateTime') {
                $singleInitializer = fn($variable) => "DateTime.parse(" . $variable . ").toLocal()";
                $outType           = 'DateTime' . ($isNull ? '?' : '');
            }

            $jsonAccess = $initializer = "json['" . $property->getName() . "']";
            if ($isArray) {
                $outType = 'List<' . $outType . '>';
                if ($isScalar) {
                    $outType .= '?';
                }
            }
            if (!$isScalar) {
                if (!$typeIsBuiltIn && $typeName != $classInfo->getClassname()) {
                    $requiredImports[] = $namespace . '/' . $typeName;
                }

                if ($isArray) {
                    $initializer = $typeName . ".listFromJsonDecoder(" . $initializer . ')';
                    if ($isNull) {
                        $initializer = $jsonAccess . ' != null ? ' . $initializer . ' : []';
                    }
                } else {
                    $initializer = $singleInitializer($initializer);
                    if ($isNull) {
                        $initializer = $jsonAccess . ' != null ? ' . $initializer . ' : null';
                    }
                }
            }
            $initializers[] = $property->getName() . " = " . $initializer;

            $code .= '  ' . $outType . ' ' . $property->getName() . ";\n";
        }
        $hasId = array_key_exists('id', $toMapDirect);
        if (!$hasId) {
            $code .= "String? get id => null;\n";
        }
        $code .= "\n  static " . $classInfo->getClassname() . " fromJsonDecoder(json) => " . $classInfo->getClassname() . ".fromJson(json);\n";
        $code .= "\n  static List<" . $classInfo->getClassname() . "> listFromJsonDecoder(list) => " .
            "List<" . $classInfo->getClassname() . ">.from(list.map((i) => " . $classInfo->getClassname() . ".fromJson(i)));\n";
        if ($hasId) {
            $code .= "\n  " . $classInfo->getClassname() . ".fromId(" . $toMapDirect['id'][2] . ($toMapDirect['id'][1] ? '?' : '') . " id) : this.fromJson({\"id\": id});\n";
        }
        $code .= "\n  " . $classInfo->getClassname() . ".fromJson(JsonKeyValue json)\n      : ";
        $code .= join(",\n        ", $initializers) . $superInitializer;
        $code .= ";\n";

        $code .= "\n  Map<String, dynamic> toMap() {\n    Map<String, dynamic> map = {\n";
        foreach ($toMapDirect as $name => $_) {
            [$isArray, $isNull] = $_;
            if ($isArray && $isNull) {
                $code .= "      \"" . $name . "\": " . $name . ".map((e) => e!.id).toList(),\n";
            } else {
                $code .= "      \"" . $name . "\": " . $name . ",\n";
            }
        }
        $code .= "    };\n";
        foreach ($toMapRecursive as $name) {
            $code .= "    if ($name != null) {\n" .
                "      var subMap = $name!.toMap();\n" .
                "      if(subMap.containsKey(\"id\")) map[\"$name\"] = subMap[\"id\"]; \n" .
                "      map.addEntries(subMap.entries.map((e) => MapEntry<String, dynamic>(\"$name.\" + e.key, e.value)));\n" .
                "    }\n";
        }
        $code .= "    return map;\n  }\n";
        $code .= "}\n";


        return new GeneratedCode(
            $this->generateImports($requiredImports, $classInfo->getNamespace()) . "\n" . $code,
            Str::snake($classInfo->getNamespace()) . '/' . Str::snake($classInfo->getClassname()) . '.dart'
        );
    }

    private function generateImports(
        array   $requiredImports,
        ?string $currentNamespace = null,
        string  $pathPrefix = '..'
    ): string
    {
        foreach ($requiredImports as &$imp) {
            if (preg_match_all('/(.*)<(.*)>/', $imp, $matches)) {
                $imp               = $matches[1][0];
                $requiredImports[] = $matches[2][0];
            }
        }

        $imports = '';
        foreach (array_unique($requiredImports) as $import) {
            if (str_ends_with($import, '[]')) {
                $import = substr($import, 0, -2);
            }
            $imports .= "import '";
            if (dirname($import) != $currentNamespace) {
                $imports .= $pathPrefix . '/';
                $dir     = Str::snake(dirname($import));
                if ($dir != '.') {
                    $imports .= $dir . '/';
                }
            } else {
                $imports .= './';
            }
            if (str_contains($import, '/')) {
                $import = basename($import);
            }
            $imports .= Str::snake($import) . ".dart';\n";
        }
        return $imports;
    }

    private function mapScalar(string $type)
    {
        $map = [
            'string'  => 'String',
            'float'   => 'double',
            'integer' => 'int',
            'mixed'   => 'dynamic',
            'boolean' => 'bool',
        ];
        if (array_key_exists($type, $map)) {
            return $map[$type];
        }
        return $type;
    }

    private function resolveType(array $types): array
    {
        if (
            count($types) == 1 ||
            (count($types) == 2 && count(filter($types, fn(ExportedType $t) => $t->getName() == 'null')) == 1)
        ) {
            $exportedType = $types[0];
            if (count($types) == 2) {
                $exportedType = filter($types, fn(ExportedType $t) => $t->getName() != 'null')[0];
            }
            $name    = $exportedType->getName();
            $isArray = false;
            if (str_ends_with($name, '[]')) {
                $name    = substr($name, 0, -2);
                $isArray = true;
            }
            if ($exportedType->isScalar()) {

                $isNull = true;
                if ($name == 'array') {
                    $name    = 'dynamic';
                    $isArray = true;
                    $isNull  = false;
                }
                if ($name == 'void') {
                    $isNull = false;
                }

                return [$this->mapScalar($name), true, $isArray, $isNull, null, null];
            }
            $class         = NameConverter::convertClassName($exportedType->getName());
            $typeClassname = $class['name'];
            if ($isArray) {
                $typeClassname = substr($typeClassname, 0, -2);
            }

            return [$typeClassname, false, $isArray, true, $class['namespace'], $class['template']];
        }
        // name, isScalar, isArray, isNull, namespace, template
        return ['dynamic', true, false, false, null, null];
    }

    public function generateClient(ControllerInfo $controllerInfo, string $importPrefix): GeneratedCode
    {
        $requiredImports = [];
        $code            = "/// Generated client. do not edit.\nclass " . $controllerInfo->getClientClassname() . " {\n" .
            "  final HttpClient _httpClient = Get.find();\n";
        foreach ($controllerInfo->getEndpoints() as $endpoint) {
            $code .= $this->generateMethod($endpoint, $requiredImports);
        }
        $code .= "}\n";

        return new GeneratedCode(
            "import 'package:dio/dio.dart';\n" .
            "import 'package:get/get.dart';\n" .
            "import '../http_client.dart';\n" .
            (str_contains($code, 'ApiFormData') ? "import './api_form_data.dart';\n" : '') .
            (str_contains($code, 'FormResponse') ? "import './form_response.dart';\n" : '') .
            (str_contains($code, 'Uint8List') ? "import 'dart:typed_data';\n" : '') .
            (str_contains($code, 'JsonKeyValue') ? "import '$importPrefix/types.dart';\n" : '') .
            $this->generateImports($requiredImports, null, $importPrefix) . "\n"
            . $code,
            Str::snake($controllerInfo->getClientClassname()) . '.dart'
        );
    }

    private function replaceReservedMethodNames(string $methodName): string
    {
        $reserved = [
            'new' => 'create',
        ];
        if (array_key_exists($methodName, $reserved)) {
            return $reserved[$methodName];
        }
        return $methodName;
    }

    private function generateMethod(ApiEndpoint $endpoint, array &$requiredImports): string
    {
        $method                   = strtolower($endpoint->getHttpMethods()[0]);
        $methodName               = $this->replaceReservedMethodNames($endpoint->getClientMethodName());
        $isWritingRequestWithData = in_array($method, ['post', 'put']);
        $passThruBody             = $isWritingRequestWithData && !$endpoint->hasRequestParameters();
        $writeRequestParameters   = $isWritingRequestWithData && $endpoint->hasRequestParameters();

        [$parameters, $namedParameters] = $this->buildParameters($endpoint, $passThruBody);
        $namedParameters[] = 'Map<String, dynamic>? $query';
        if ($isWritingRequestWithData) {
            $namedParameters[] = 'ProgressCallback? $onSendProgress';
        }
        if ($isWritingRequestWithData || $method == 'get') {
            $namedParameters[] = 'ProgressCallback? $onReceiveProgress';
        }

        if ($endpoint->getGenerateClient()->specialType == GenerateClient::FORM) {
            $returnTypeName        = 'FormResponse';
            $decoder               = 'FormResponse.fromResponse';
            $returnIsScalar        = false;
            $returnTemplate        = false;
            $passResponseToDecoder = true;
        } else {
            $resolvedReturnType = $this->resolveType($endpoint->getReturnTypes());
            [$decoderClass, $returnIsScalar, $returnIsArray, , , $returnTemplate] = $resolvedReturnType;
            $returnTypeName = $this->resolveReturnTypeName($resolvedReturnType, $requiredImports);

            if ($returnTemplate) {
                $decoderMethod = 'fromJson';
            } else if ($returnIsArray) {
                $decoderMethod = 'listFromJsonDecoder';
            } else {
                $decoderMethod = 'fromJsonDecoder';
            }

            $decoder               = $decoderClass . '.' . $decoderMethod;
            $passResponseToDecoder = false;
        }

        if (!in_array($returnTypeName, ['dynamic', 'void', 'Uint8List'])) {
            $returnTypeName .= '?';
        }
        $code = $this->generateSignature($returnTypeName, $methodName, $parameters, $namedParameters);

        $code .= $this->generateNormalizationBlock($endpoint, $requiredImports);
        if ($passThruBody) {
            $code   .= $this->generateFormBlock($method);
            $method = 'post';
        }

        if ($endpoint->hasQueryParameters()) {
            $code .= "final queryParameters = Map<String, dynamic>.from(\$query ?? {});\n";
            foreach ($endpoint->getQueryParameters() as $queryParameter) {
                $name = $queryParameter->getName();
                if ($queryParameter->isOptional()) {
                    $code .= "if($name != null) ";
                }
                $code .= "queryParameters[\"$name\"] = $name;\n";
            }
        }

        $requestCode = $this->generateHttpRequestCode($method, $passThruBody, $writeRequestParameters, $endpoint, $returnTypeName, $isWritingRequestWithData, $passResponseToDecoder);

        if (!$returnIsScalar) {
            if ($returnTemplate) {
                $code .= "return $decoder($requestCode, " . $returnTemplate['name'] . ".listFromJsonDecoder);";
            } else {
                $code .= "return $decoder($requestCode);";
            }
        } else {
            $code .= "return $requestCode;";
        }

        $code .= "  }\n";

        return $code;
    }

    private function buildParameters(ApiEndpoint $endpoint, bool $passThruBody): array
    {
        $parameters      = [];
        $namedParameters = [];
        foreach ($endpoint->getAllParameters() as $parameter) {
            $parameterType = $parameter->getType()->isScalar() ? $this->mapScalar($parameter->getType()->getName()) : 'dynamic';
            if (!$parameter->isOptional()) {
                $parameters[] = $parameterType . ' ' . $parameter->getName();
            } else {
                if ($parameterType != 'dynamic') $parameterType .= '?';
                $namedParameters[] = $parameterType . ' ' . $parameter->getName();
            }
        }
        if ($passThruBody) {
            $parameters[] = 'dynamic body';
        }
        return [$parameters, $namedParameters];
    }

    private function generateSignature(
        string $returnTypeName,
        string $methodName,
        array  $parameters,
        array  $namedParameters
    ): string
    {
        $code = "\n  Future<" . $returnTypeName . '> ' . $methodName . "(";
        if (count($parameters)) {
            $code .= "\n    " . join(",\n    ", $parameters) . ", {";
        } else {
            $code .= "{";
        }
        $code .= "\n    " . join(",\n    ", $namedParameters) . "\n  }) async {\n";
        return $code;
    }

    private function generateNormalizationBlock(ApiEndpoint $endpoint, array &$requiredImports): string
    {
        $code = '';
        /** @var ApiParameter $parameter */
        foreach ([...$endpoint->getPathParameters(), ...$endpoint->getQueryParameters()] as $parameter) {
            if ($parameter->getType()->isScalar()) continue;
            $name = $parameter->getName();
            if ($name == 'body') { // reserved variable names for named parameters
                $name = $name . '_';
            }
            [$typeName, , , , $namespace] = $this->resolveType([$parameter->getType()]);
            $requiredImports[] = $namespace . '/' . $typeName;
            $code              .= "    if ($name is $typeName) {\n        $name = $name.id;\n    }" .
                "    if (" . ($parameter->isOptional() ? "$name != null &&" : '') . " $name is! String) {\n" .
                "        throw Exception('$name must be String or $typeName');\n" .
                "    }\n";
        }
        return $code;
    }

    private function resolveReturnTypeName(array $resolvedReturnType, array &$requiredImports): string
    {
        [$returnTypeName, $returnIsScalar, $returnIsArray, , $namespace, $returnTemplate] = $resolvedReturnType;
        if (!$returnIsScalar) {
            $requiredImports[] = $namespace . '/' . $returnTypeName;
            if ($returnTemplate) {
                $requiredImports[] = $returnTemplate['namespace'] . '/' . $returnTemplate['name'];
                $returnTypeName    .= '<' . $returnTemplate['name'] . '>';
            }
        }
        if ($returnTypeName == 'dynamic' && $returnIsArray) {
            $returnTypeName = 'JsonKeyValue';
        } else if ($returnIsArray) {
            $returnTypeName = 'List<' . $returnTypeName . '>';
        } else if ($returnTypeName == 'binary') {
            $returnTypeName = 'Uint8List';
        }
        return $returnTypeName;
    }

    private function generateFormBlock($method): string
    {
        return "    if(body is Map<String, dynamic>) {\n" .
            "      body = await ApiFormData.create(body" . ($method != 'post' ? ", '" . $method . "'" : '') . ");\n" .
            "    }\n";
    }

    private function generateHttpRequestCode(
        string      $method,
        bool        $passThruBody,
        bool        $writeRequestParameters,
        ApiEndpoint $endpoint,
        string      $returnTypeName,
        bool        $isWritingRequestWithData,
        bool        $passResponseToDecoder
    ): string
    {
        $path = preg_replace('/\{(.*?)\}/', '\\$$1', $endpoint->getPath());

        $requestCode = "(await _httpClient.$method('$path',";
        if ($passThruBody) {
            $requestCode .= " data: body,";
        }
        if ($writeRequestParameters) {
            $requestCode .= " data: {\n";
            foreach ($endpoint->getRequestParameters() as $parameter) {
                $requestCode .= " '" . $parameter->getName() . "': " . $parameter->getName() . ",";
            }
            $requestCode .= "},";
        }
        if ($endpoint->hasQueryParameters()) {
            $requestCode .= " queryParameters: queryParameters";
        } else {
            $requestCode .= " queryParameters: \$query";
        }
        if ($returnTypeName == "Uint8List") {
            $requestCode .= ", options: Options(responseType: ResponseType.bytes)";
        }
        if ($isWritingRequestWithData) {
            $requestCode .= ", onSendProgress: \$onSendProgress";
        }
        if ($isWritingRequestWithData || $method == 'get') {
            $requestCode .= ", onReceiveProgress: \$onReceiveProgress";
        }
        if ($endpoint->getGenerateClient()->specialType == GenerateClient::FORM) {
            $requestCode .= ", options: Options(validateStatus: (status) => status != null && ((status >= 200 && status < 300) || status == 422))";
        }
        $requestCode .= "))";
        if (!$passResponseToDecoder) {
            $requestCode .= ".data";
        }
        return $requestCode;
    }

    public function generateEnum(EnumInfo $classInfo): GeneratedCode
    {
        // TODO: Implement generateEnum() method.
    }
}
