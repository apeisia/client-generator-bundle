import 'package:ccvd_app/models/gen/types.dart';
import 'package:dio/dio.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

/// Generated. do not edit.
class FormResponse {
  final FormErrorModel errorModel;
  final Response? response;
  String? id;

  static FormResponse fromResponse(Response response) {
    if (response.statusCode == 204) {
      // no content
      return FormResponse.empty();
    }
    var json = response.data;
    if (json is JsonKeyValue) {
      return FormResponse.fromJsonResponse(json, response);
    }
    throw Exception("Unexpected response in FormResponse");
  }

  FormResponse.fromJsonResponse(JsonKeyValue json, Response this.response)
      : errorModel = FormErrorModel.fromJson(json),
        id = json["id"];

  FormResponse.empty()
      : errorModel = FormErrorModel.fromJson(null),
        response = null;

  get hasErrors {
    return errorModel.hasErrors || (response != null && response!.statusCode == 422);
  }

  void applyErrors(FormBuilderState formBuilderState) {
    errorModel.errors.forEach((key, value) {
      formBuilderState.invalidateField(name: key, errorText: value);
    });
  }
}

class FormErrorModel {
  Map<String, String> errors = {};
  Map<String, FormErrorModel> subErrors = {};

  FormErrorModel.fromJson(JsonKeyValue? json) {
    if (json == null) return;

    json.forEach((key, value) {
      if (key == "form" && value is Map && value["children"] != null) {
        flatten(JsonKeyValue.from(value), "");
      }
    });
  }

  flatten(JsonKeyValue json, prefix) {
    json["children"].forEach((fieldName, errorList) {
      if (errorList["children"] is JsonKeyValue) {
        flatten(errorList, fieldName + ".");
      } else if (errorList["errors"] is List && (errorList["errors"] as List).isNotEmpty) {
        errors[prefix + fieldName] = (errorList["errors"] as List).join(" ");
      }
    });
  }

  get hasErrors {
    return subErrors.entries
        .fold<bool>(errors.isNotEmpty, (previousValue, element) => previousValue || element.value.hasErrors);
  }
}
