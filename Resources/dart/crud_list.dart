import 'package:get/get.dart';

/// Generated. do not edit.
class CrudList<T> {
  List<T> items = [];

  int page;
  int itemsPerPage;
  int totalItems;
  int pageCount;

  CrudList.fromJson(Map<String, dynamic> json, Decoder<List<T>> decoder)
      : items = json['list'] != null ? decoder(json['list']) : [],
        page = json["page"],
        itemsPerPage = json["itemsPerPage"],
        totalItems = json["totalItems"],
        pageCount = json["pageCount"];
}
