import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart' as image;
import 'dart:typed_data';
import 'package:file_picker/file_picker.dart';


/// Generated. do not edit.
class ApiFormData {
  static Future<FormData> create(Map<String, dynamic> map, [String? method]) async {
    var newMap = <String, dynamic>{};
    if (method != null) {
      newMap["_method"] = method;
    }
    for (var entry in map.entries) {
      await _populateMap(entry.key, entry.value, newMap);
    }
    return FormData.fromMap(newMap);
  }

  static Future<void> _populateMap(String key, dynamic value, Map<String, dynamic> newMap) async {
    if (key.contains(RegExp(r"\."))) {
      // a.b.c => a[b][c]
      key = key.replaceFirst(".", "[").replaceAll(".", "][") + "]";
    }

    if (value == null) return;

    if (value is bool) {
      // symfony form: key exists = true, no key = false
      if (value) {
        newMap[key] = "1";
      }
    } else if (value is XFile) {
      if (value.path != "") {
        newMap[key] = await MultipartFile.fromFile(value.path);
      } else {
        newMap[key] = MultipartFile.fromBytes(await value.readAsBytes(),
            contentType: value.mimeType != null ? MediaType.parse(value.mimeType!) : null, filename: key + ".jpg");
      }
    } else if (value is image.Image) {
      newMap[key] = MultipartFile.fromBytes(image.encodeJpg(value),
          contentType: MediaType.parse("image/jpg"), filename: key + ".jpg");
    } else if (value is PlatformFile) {
      newMap[key] = MultipartFile.fromBytes(value.bytes!,
          contentType: MediaType.parse("application/octet-stream"), filename: value.name);
    } else if (value is List || value is Set) {
      var i = 0;
      for (var x in value) {
        await _populateMap(key + "[" + i.toString() + "]", x, newMap);
        i++;
      }
    } else if (value is Map) {
      for (var x in value.entries) {
        await _populateMap(key + "[" + x.key + "]", x.value, newMap);
      }
    } else {
      newMap[key] = value;
    }
  }
}
