<?php

namespace Apeisia\ClientGeneratorBundle\CodeReader;

use Illuminate\Support\Str;
use Roave\BetterReflection\Reflection\ReflectionClass as BetterReflectionClass;

abstract class NameConverter
{

    /**
     * @param BetterReflectionClass|\ReflectionClass|string $class
     * @return string
     */
    public static function getName($class): string
    {
        return trim(join('/', self::convertClassName($class)), '/');
    }

    /**
     * @param BetterReflectionClass|\ReflectionClass|string $class
     * @return string
     */
    public static function getNamespace($class): ?string
    {
        return self::convertClassName($class)['namespace'];
    }

    /**
     * @param BetterReflectionClass|\ReflectionClass|string $class
     * @return string
     */
    public static function getClassname($class): string
    {
        return self::convertClassName($class)['name'];
    }

    /**
     * @param BetterReflectionClass|\ReflectionClass|string $class
     * @return array
     */
    public static function convertClassName($class): array
    {
        $isArray = false;
        if ($class instanceof \ReflectionClass || $class instanceof BetterReflectionClass) {
            $class = $class->getName();
        } else if (str_ends_with($class, '[]')) {
            $isArray = true;
            $class   = substr($class, 0, -2);
        }
        $template = null;

        if (preg_match_all('/^(.*)<(.*)>$/', $class, $matches)) {
            $class    = $matches[1][0];
            $template = self::convertClassName($matches[2][0]);
        }

        $namespace = null;
        if (preg_match('/^(.*?)(Bundle)?\\\(Entity|Model)/', $class, $matches)) {
            $namespace = $matches[1];
        }
        $shortName = Str::afterLast($class, '\\');
        return [
            'namespace' => $namespace,
            'name'      => $shortName . ($isArray ? '[]' : ''),
            'template'  => $template,
        ];
    }

}
