<?php

namespace Apeisia\ClientGeneratorBundle\CodeReader;

use Apeisia\AccessorTraitBundle\Model\ResolvedType;
use Apeisia\AccessorTraitBundle\Resolver\TypeResolver;
use Apeisia\ClientGeneratorBundle\Event\ReadModelEvent;
use Apeisia\ClientGeneratorBundle\Model\ExportedProperty;
use Apeisia\ClientGeneratorBundle\Model\ExportedType;
use Apeisia\ClientGeneratorBundle\Model\ModelInfo;
use Apeisia\WatchBundle\Annotation\AnnotationAndAttributeReader;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JmsSerializer;
use Psr\EventDispatcher\EventDispatcherInterface;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionMethod as BetterReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionProperty;
use Symfony\Component\Serializer\Annotation as SymfonySerializer;

class ModelReader
{
    public function __construct(
        private readonly TypeResolver             $typeResolver,
        private readonly EventDispatcherInterface $eventDispatcher
    )
    {
    }

    public function read(ReflectionClass $class): ModelInfo
    {
        return new ModelInfo($this->readClass($class), $class);
    }

    /**
     * @throws \ReflectionException
     */
    private function readClass(ReflectionClass $class): array
    {
        if ($class->isTrait())
            return [];

        $annotationReader = new AnnotationAndAttributeReader();

        $exportedProperties = [];

        foreach ($annotationReader->getClassAnnotations($class) as $classAnnotation) {
            if ($classAnnotation instanceof JmsSerializer\VirtualProperty) {
                $groupsAnnotation = filter($classAnnotation->options, fn($annotation
                ) => $annotation instanceof JmsSerializer\Groups);
                if (count($groupsAnnotation)) {
                    $exportedProperties[$classAnnotation->name] = new ExportedProperty($classAnnotation->name, $groupsAnnotation[0]->groups, [], $class->getAttributes(), $class);
                }
            }
        }

        foreach ($class->getProperties() as $property) {
            if (!$this->shouldInclude($property->getDeclaringClass(), $class->getName())) {
                continue;
            }

            /** @var JmsSerializer\Groups $jmsGroups */
            $jmsGroups = $annotationReader->getPropertyAnnotation($property, JmsSerializer\Groups::class);
            if ($jmsGroups && (count($jmsGroups->groups) > 1 || $jmsGroups->groups[0] != 'Default')) {
                $exportedProperties[$property->getName()] = new ExportedProperty(
                    $property->getName(),
                    $jmsGroups->groups,
                    $this->resolvePropertyType($property),
                    $property->getAttributes(),
                    $class,
                );
            }

            /** @var SymfonySerializer\Groups $symfonySerializer */
            $symfonySerializer = $annotationReader->getPropertyAnnotation($property, SymfonySerializer\Groups::class);
            if ($symfonySerializer && (count($symfonySerializer->getGroups()) > 1 || $symfonySerializer->getGroups()[0] != 'Default')) {
                $exportedProperties[$property->getName()] = new ExportedProperty(
                    $property->getName(),
                    $symfonySerializer->getGroups(),
                    $this->resolvePropertyType($property),
                    $property->getAttributes(),
                    $class,
                );
            }

        }

        foreach ($class->getMethods() as $method) {
            if (!$this->shouldInclude($method->getDeclaringClass(), $class->getName())) {
                continue;
            }

            $virtualProperty = $annotationReader->getMethodAnnotation($method, JmsSerializer\VirtualProperty::class);
            $jmsGroups       = $annotationReader->getMethodAnnotation($method, JmsSerializer\Groups::class);
            if ($virtualProperty && $jmsGroups) {
                $name = $method->getName();
                if (str_starts_with($name, 'get')) {
                    $name = lcfirst(substr($name, 3));
                }

                $exportedProperties[$name] = new ExportedProperty($name, $jmsGroups->groups, $this->resolveMethodType($method), $method->getAttributes(), $class);

            }
        }

        $event = new ReadModelEvent($this, $class);
        $this->eventDispatcher->dispatch($event);
        foreach ($event->getExportedProperties() as $exportedProperty) {
            $exportedProperties[$exportedProperty->getName()] = $exportedProperty;
        }

        return $exportedProperties;
    }

    private function shouldInclude(ReflectionClass $declaringClass, string $className): bool
    {
        return $declaringClass->getName() === $className ||
            $declaringClass->isTrait() ||
            str_contains($declaringClass->getFileName(), '/vendor/');
    }

    public function resolvePropertyType(ReflectionProperty $property): array
    {
        return $this->resolveType($this->typeResolver->resolvePropertyType($property));
    }

    public function resolveMethodType(BetterReflectionMethod $method): array
    {
        return $this->resolveType($this->typeResolver->resolveMethodType($method));
    }

    /**
     * @param ResolvedType $inputType
     * @return string[]
     */
    private function resolveType(ResolvedType $inputType): array
    {
        $declaredType = null;

        if ($inputType->hasPhpDocType()) {
            $declaredType = $inputType->getPhpDocType();
        } else if ($inputType->hasSignatureType()) {
            $declaredType = $inputType->getSignatureType();
        }
        $resolvedTypes         = [];
        $removedCollectionType = false;
        $hasArrayType          = false;

        foreach (explode('|', $declaredType) as $type) {
            if (empty($type))
                continue;

            if ($type[0] == '?') {
                $resolvedTypes[] = new ExportedType('null', true);
                $type            = substr($type, 1);
            }

            $isArrayDeclaration = str_ends_with($type, '[]');
            if ($isArrayDeclaration) {
                $type         = substr($type, 0, -2);
                $hasArrayType = true;
            }
            if ($type == 'array') {
                $hasArrayType = true;
            }

            $import = filter(
                $inputType->getImports(),
                fn($import) => str_ends_with($import->getFqcn(), '\\' . $type) || $type === $import->getAlias());

            if (count($import)) {
                $type = $import[0]->getFqcn();
            }

            $isScalar = !(class_exists($type) || interface_exists($type));

            if ($isArrayDeclaration) {
                $type .= '[]';
            }
            if (
                (class_exists($type) && array_key_exists(Collection::class, class_implements($type)))
                ||
                $type == Collection::class || $type == '\\' . Collection::class
                ||
                str_ends_with($type, 'ArrayCollection')
            ) {
                // do not add collections, but add an array in the end if this was the only array type
                $removedCollectionType = true;
                continue;
            }

            $resolvedTypes[] = new ExportedType($type, $isScalar);
        }

        $resolvedTypes = array_unique($resolvedTypes);
        if ($removedCollectionType && (count($resolvedTypes) == 0 || !$hasArrayType)) {
            $resolvedTypes[] = new ExportedType('array', true);
        }

        return $resolvedTypes;
    }
}
