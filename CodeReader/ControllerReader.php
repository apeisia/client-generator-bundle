<?php

namespace Apeisia\ClientGeneratorBundle\CodeReader;

use Apeisia\WatchBundle\Annotation\AnnotationAndAttributeReader;
use Apeisia\ClientGeneratorBundle\Annotation\ClientQueryParameter;
use Apeisia\ClientGeneratorBundle\Annotation\ClientRequestParameter;
use Apeisia\ClientGeneratorBundle\Annotation\GenerateClient;
use Apeisia\ClientGeneratorBundle\Model\ApiEndpoint;
use Apeisia\ClientGeneratorBundle\Model\ApiParameter;
use Apeisia\ClientGeneratorBundle\Model\ControllerInfo;
use Apeisia\ClientGeneratorBundle\Model\ExportedType;
use Apeisia\ClientGeneratorBundle\Notify\NotifySend;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\View\View;
use Roave\BetterReflection\Reflection\Adapter\ReflectionClass as ReflectionClassAdapter;
use Roave\BetterReflection\Reflection\Adapter\ReflectionMethod as ReflectionMethodAdapter;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class ControllerReader
{

    public function read(ReflectionClass $class): ControllerInfo
    {
        return new ControllerInfo($this->readClass($class), $class);
    }

    private function readClass(ReflectionClass $class): array
    {
        $annotationReader = new AnnotationAndAttributeReader();

        $prefix = '';

        $endpoints = [];

        if ($restPrefix = $annotationReader->getClassAnnotation(new ReflectionClassAdapter($class), Prefix::class)) {
            $prefix = $restPrefix->value;
        }
        if ($routePrefix = $annotationReader->getClassAnnotation(new ReflectionClassAdapter($class), Route::class)) {
            $prefix = $routePrefix->getPath();
        }

        foreach ($class->getMethods() as $method) {
            if (!$method->isPublic()) {
                continue;
            }
            $endpoints[] = $this->readMethod($annotationReader, $prefix ?? '', $method);
        }

        return filter($endpoints, fn($endpoint) => (bool)$endpoint);
    }

    private function readMethod(
        AnnotationAndAttributeReader $annotationReader,
        string                       $prefix,
        ReflectionMethod             $method
    ): ?ApiEndpoint
    {
        /** @var GenerateClient $generateClient */
        $generateClient = $annotationReader->getMethodAnnotation(new ReflectionMethodAdapter($method), GenerateClient::class);
        if (!$generateClient) {
            return null;
        }
        /** @var Route $route */
        $route = $annotationReader->getMethodAnnotation(new ReflectionMethodAdapter($method), Route::class);
        if (!$route) {
            return null;
        }
        $path              = $prefix . $route->getPath();
        $pathParameters    = $this->readPathParameters($path, $method, $generateClient);
        $queryParameters   = $this->readQueryParameterAnnotations($method, $annotationReader);
        $requestParameters = $this->readRequestParameterAnnotations($method, $annotationReader);

        [$returnType, $isScalar] = $this->resolveReturnType($method, $generateClient);

        if (!$returnType) {
            $this->crash('Endpoint', 'Endpoint ' . $method->getName() . ' misses required return type');
            return null;
        }


        return new ApiEndpoint(
            $method,
            $generateClient,
            $path,
            $pathParameters,
            $queryParameters,
            $requestParameters,
            $route->getMethods(),
            [
                new ExportedType($returnType, $isScalar)
            ]
        );
    }

    private function crash($title, $message)
    {
        NotifySend::send('ClientGenerator', $title, $message);
        throw new \RuntimeException($message);
    }

    private function readPathParameters(string $path, ReflectionMethod $method, GenerateClient $generateClient): array
    {
        preg_match_all('/\{(.*?)\}/', $path, $matches);
        $pathParameters = [];
        foreach ($matches[1] as $variable) {
            $parameter = $method->getParameter($variable);

            if (!$parameter) {
                $this->crash('ClientGenerator', 'URL parameter "' . $variable . '" in method "' . $method->getDeclaringClass()->getName() . ':' . $method->getName() . '" not defined in method signature.');
            }

            $type = $parameter->getType();

            $pathParameters[] = new ApiParameter($variable, new ExportedType($type->getName(), $type->isBuiltin()));
        }
        return $pathParameters;
    }

    private function readQueryParameterAnnotations(ReflectionMethod $method, AnnotationAndAttributeReader $annotationReader): array
    {
        $queryParameters = [];
        foreach ($annotationReader->getMethodAnnotations(new ReflectionMethodAdapter($method)) as $annotation) {
            if ($annotation instanceof ClientQueryParameter) {
                $queryParameters[] = new ApiParameter($annotation->name, new ExportedType($annotation->type), $annotation->optional);
            }
        }
        return $queryParameters;
    }

    private function readRequestParameterAnnotations(ReflectionMethod $method, AnnotationAndAttributeReader $annotationReader): array
    {
        $requestParameters = [];
        foreach ($annotationReader->getMethodAnnotations(new ReflectionMethodAdapter($method)) as $annotation) {
            if ($annotation instanceof ClientRequestParameter) {
                $requestParameters[] = new ApiParameter($annotation->name, new ExportedType($annotation->type), $annotation->optional);
            }
        }
        return $requestParameters;
    }

    private function resolveReturnType(ReflectionMethod $method, GenerateClient $generateClient): array
    {
        if ($generateClient->specialType == GenerateClient::LIST) {
            return ['BaseBundle\\CrudList<' . $generateClient->returnType . '>', false];
        }
        if ($generateClient->returnType) {
            return [$generateClient->returnType, !str_contains($generateClient->returnType, '\\')];
        }

//        $docReturnTypes = $method->getDocBlockReturnTypes();
//        if (in_array('\\' . View::class, $docReturnTypes) && in_array('array', $docReturnTypes) && count($docReturnTypes) == 2) {
//            // we assume View::class as array anyway, so support this special case
//            $docReturn = 'array';
//        } else {
//            $docReturn = count($method->getDocBlockReturnTypes()) == 1 ? (string)$method->getDocBlockReturnTypes()[0] : null;
//        }
        $docReturn = null;
        $phpReturn = $method->getReturnType();
        if ($phpReturn) {
            if (method_exists($phpReturn, 'getTypes')) {
                foreach ($phpReturn->getTypes() as $type) {
                    if ($type->getName() != 'null') {
                        $phpReturn = $type;
                        break;
                    }
                }
            }
        }
        $return   = $phpReturn?->getName();
        $isScalar = $phpReturn?->isBuiltin();
        if ($docReturn && (!$return || $return == 'array')) {
            $isScalar = !str_contains($docReturn, '\\');
            if (str_starts_with($docReturn, '\\')) {
                $return = substr($docReturn, 1);
            } else {
                $return = $docReturn;
            }
        }
        if ($return == View::class) {
            $return   = 'array';
            $isScalar = true;
        }
        if (in_array($return, [StreamedResponse::class, Response::class])) {
            $return   = 'mixed';
            $isScalar = true;
        }
        return [$return, $isScalar];
    }

}
