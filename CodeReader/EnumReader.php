<?php

namespace Apeisia\ClientGeneratorBundle\CodeReader;

use Apeisia\ClientGeneratorBundle\Model\EnumInfo;
use Roave\BetterReflection\Reflection\ReflectionEnum;

class EnumReader
{
    public function read(ReflectionEnum $reflectionClass): EnumInfo
    {
        return new EnumInfo($reflectionClass);
    }
}
