<?php

namespace Apeisia\ClientGeneratorBundle\Notify;

abstract class NotifySend
{
    public static function send(string $appName, string $title, string $message, int $time = 0): void
    {
        if (file_exists('/usr/bin/notify-send')) {
            system('/usr/bin/notify-send -a ' . escapeshellarg($appName) . ' ' . escapeshellarg($title) . ' ' . escapeshellarg($message)) . ' -t ' . $time;
            return;
        }
        $fp = @fsockopen('172.17.0.1', 1216);
        if ($fp) {
            fwrite($fp, json_encode([
                'app_name' => $appName,
                'summary' => $title,
                'body' => $message,
                'expire_time' => $time
            ]));
            fclose($fp);
            return;
        }

        echo 'Could not send notify: ' . $title . ': ' . $message . "\n";
    }
}
