<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

/**
 * Marks a property as required in the generated TypeScript code (effectively removing the `?` from the type).
 * Use this if you are sure that the property is always present in every instance of the class.
 */
#[\Attribute]
class TypescriptRequired
{
}
