<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

use Attribute;

/**
 * @Annotation
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class ClientQueryParameter
{
    public function __construct(public string $name, public string $type, public ?string $description = null, public bool $optional = false)
    {
    }
}
