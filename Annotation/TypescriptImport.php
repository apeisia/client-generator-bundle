<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

/**
 * Adds an import to the given class in the generated TypeScript code. Class name must be a fully qualified class name.
 *
 * To import a generated PHP Model, use the `model` parameter.
 * To import a specific typescript type, use the `importName` and `importFile` parameter.
 *
 * Example:
 * #[TypescriptImport(model: User::class)]
 *
 * #[TypescriptImport(name: 'default', alias: 'QuillType', file: 'quill')]
 *   Results in: import {default as QuillType} from 'quill';
 *
 * #[TypescriptImport(name: 'AppConfig', file: '@/app-config')]
 *   Results in: import {AppConfig} from '@/app-config';
 */
#[\Attribute]
class TypescriptImport
{
    /**
     * @param class-string $class
     */
    function __construct(
        public ?string $model = null,
        public ?string $name = null,
        public ?string $alias = null,
        public ?string $file = null,
    )
    {
        if ($this->model && $this->name) {
            throw new \InvalidArgumentException('Only one of `model` or `name` can be set.');
        }

        if (!$this->model && !$this->name) {
            throw new \InvalidArgumentException('Either `model` or `name` must be set.');
        }

        if ($this->name && !$this->file) {
            throw new \InvalidArgumentException('`file` must be set when `name` is set.');
        }
    }
}
