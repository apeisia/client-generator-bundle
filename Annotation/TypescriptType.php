<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

/**
 * Allows to override the type of the property in the generated TypeScript code. This takes a literal string that
 * will be used as the type in the generated code as-is. To add imports, use {@link TypescriptImport} attribute.
 */
#[\Attribute]
class TypescriptType
{
    function __construct(public string $type)
    {
    }
}
