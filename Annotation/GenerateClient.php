<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

use Attribute;

/**
 * @Annotation
 */
#[Attribute(Attribute::TARGET_METHOD)]
class GenerateClient
{
    const LIST = 'list';
    const FORM = 'form';


    public function __construct(

        /**
         * One of the constants above
         */
        public readonly ?string $specialType = null,


        /**
         * Allows overwriting the php or phpdoc return type without getting IDE warnings
         *
         * Can be a PHP type or "binary"
         */
        public readonly ?string $returnType = null)
    {
    }
}
