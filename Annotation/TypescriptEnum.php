<?php

namespace Apeisia\ClientGeneratorBundle\Annotation;

use Apeisia\ClientGeneratorBundle\Enum\TypescriptEnumType;

/**
 * When applied to a class, this annotation will generate a typescript enum instead of a union type.
 * When not set, a union type will be generated. It can be set explicitly to 'Union' to generate a union type.
 */
#[\Attribute]
class TypescriptEnum
{
    public function __construct(public TypescriptEnumType $type = TypescriptEnumType::Native)
    {
    }
}
