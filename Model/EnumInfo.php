<?php

namespace Apeisia\ClientGeneratorBundle\Model;

use Apeisia\ClientGeneratorBundle\Annotation\TypescriptEnum;
use Apeisia\ClientGeneratorBundle\CodeReader\NameConverter;
use Apeisia\ClientGeneratorBundle\Enum\TypescriptEnumType;
use Apeisia\WatchBundle\Annotation\AttributeInstanceCreator;
use Roave\BetterReflection\Reflection\ReflectionEnum;
use Roave\BetterReflection\Reflection\ReflectionEnumCase;

class EnumInfo
{
    private ReflectionEnum $class;

    public function __construct(ReflectionEnum $class)
    {
        $this->class = $class;
    }

    public function getName(): string
    {
        return NameConverter::getName($this->class);
    }

    public function getNamespace(): ?string
    {
        return NameConverter::getNamespace($this->class);
    }

    public function getClassname(): string
    {
        return NameConverter::getClassname($this->class);
    }

    public function getType()
    {
        return $this->class->getBackingType();
    }

    /**
     * @return array<string, ReflectionEnumCase>
     */
    public function getCases(): array
    {
        return $this->class->getCases();
    }

    public function getEnumType(): TypescriptEnumType
    {
        $attributes = $this->class->getAttributesByInstance(TypescriptEnum::class);

        if (count($attributes) === 0) {
            return TypescriptEnumType::Union;
        }

        /** @var TypescriptEnum $attribute */
        $attribute = AttributeInstanceCreator::createInstance($this->class, $attributes[0]);

        return $attribute->type;
    }
}
