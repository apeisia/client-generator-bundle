<?php

namespace Apeisia\ClientGeneratorBundle\Model;

use Illuminate\Support\Str;

class ExportedType
{
    private string $name;
    private bool $isScalar;

    public function __construct(string $name, ?bool $isScalar = null)
    {
        $this->name     = $name;
        if($isScalar === null) {
            $isScalar = !str_contains($name, '\\');
        }
        $this->isScalar = $isScalar;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isScalar(): bool
    {
        return $this->isScalar;
    }

    public function __toString()
    {
        return $this->name;
    }
}
