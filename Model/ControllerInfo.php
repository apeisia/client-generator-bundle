<?php

namespace Apeisia\ClientGeneratorBundle\Model;


use Roave\BetterReflection\Reflection\ReflectionClass;

class ControllerInfo
{
    /**
     * @var ApiEndpoint[]
     */
    private array $endpoints;
    private ReflectionClass $class;

    /**
     * @param ApiEndpoint[] $endpoints
     * @param ReflectionClass $class
     */
    public function __construct(array $endpoints, ReflectionClass $class)
    {
        $this->endpoints = $endpoints;
        $this->class     = $class;
    }

    /**
     * @return ApiEndpoint[]
     */
    public function getEndpoints(): array
    {
        return $this->endpoints;
    }

    public function getClass(): ReflectionClass
    {
        return $this->class;
    }

    public function hasEndpoints(): bool
    {
        return count($this->endpoints) > 0;
    }

    public function getClientClassname(): string
    {
        return str_replace('Controller', 'Api', $this->getClass()->getShortName());
    }
}
