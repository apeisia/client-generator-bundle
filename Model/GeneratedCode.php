<?php

namespace Apeisia\ClientGeneratorBundle\Model;

class GeneratedCode
{
    private string $code;
    private string $filename;

    public function __construct(string $code, string $filename) {

        $this->code = $code;
        $this->filename = $filename;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }
}
