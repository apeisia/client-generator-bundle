<?php

namespace Apeisia\ClientGeneratorBundle\Model;

class ApiParameter
{
    private string $name;
    private ExportedType $type;
    private ?string $description;
    private bool $optional;

    public function __construct(
        string       $name,
        ExportedType $type,
        bool $optional = false,
        ?string $description = null
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->optional = $optional;
        $this->description = $description;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): ExportedType
    {
        return $this->type;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
    
    public function isOptional(): bool
    {
        return $this->optional;
    }
}
