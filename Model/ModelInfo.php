<?php

namespace Apeisia\ClientGeneratorBundle\Model;

use Apeisia\ClientGeneratorBundle\CodeReader\NameConverter;
use Roave\BetterReflection\Reflection\ReflectionClass;

class ModelInfo
{
    private array $properties;
    private ReflectionClass $class;

    public function __construct(array $properties, ReflectionClass $class)
    {

        $this->properties = $properties;
        $this->class      = $class;
    }

    public function getName(): string
    {
        return NameConverter::getName($this->class);
    }

    public function getNamespace(): ?string
    {
        return NameConverter::getNamespace($this->class);
    }

    public function getClassname(): string
    {
        return NameConverter::getClassname($this->class);
    }

    public function hasParent(): bool
    {
        return $this->class->getParentClass() && !str_contains($this->class->getParentClass()->getFileName(), '/vendor/');
    }

    public function getParent(): ?string
    {
        if (!$this->hasParent()) {
            return null;
        }

        return NameConverter::getName($this->class->getParentClass());
    }

    public function getParentNamespace(): ?string
    {
        if (!$this->hasParent()) {
            return null;
        }

        return NameConverter::getNamespace($this->class->getParentClass());
    }

    public function getParentClassname(): ?string
    {
        if (!$this->hasParent()) {
            return null;
        }

        return NameConverter::getClassname($this->class->getParentClass());
    }

    /**
     * @return ExportedProperty[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    public function hasExportedProperties(): bool
    {
        return count($this->properties) > 0;
    }

    public function getClass(): ReflectionClass
    {
        return $this->class;
    }
}
