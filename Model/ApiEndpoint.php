<?php

namespace Apeisia\ClientGeneratorBundle\Model;

use Apeisia\ClientGeneratorBundle\Annotation\GenerateClient;
use Illuminate\Support\Str;
use Roave\BetterReflection\Reflection\ReflectionMethod;

class ApiEndpoint
{
    private string $path;
    private ReflectionMethod $reflectionMethod;
    private GenerateClient $generateClient;

    /**
     * @var string[]
     */
    private array $httpMethods;

    /**
     * @var ExportedType[]
     */
    private array $returnTypes;

    /**
     * @var ApiParameter[]
     */
    private array $pathParameters;

    /**
     * @var ApiParameter[]
     */
    private array $queryParameters;

    /**
     * @var ApiParameter[]
     */
    private array $requestParameters;

    /**
     * @param ReflectionMethod $reflectionMethod
     * @param string $path
     * @param ApiParameter[] $queryParameters
     * @param ApiParameter[] $requestParameters
     * @param string[] $httpMethods
     * @param ExportedType[] $returnTypes
     */
    public function __construct(
        ReflectionMethod $reflectionMethod,
        GenerateClient   $generateClient,
        string           $path,
        array            $pathParameters,
        array            $queryParameters,
        array            $requestParameters,
        array            $httpMethods,
        array            $returnTypes
    ) {
        $this->reflectionMethod  = $reflectionMethod;
        $this->generateClient    = $generateClient;
        $this->path              = $path;
        $this->httpMethods       = $httpMethods;
        $this->returnTypes       = $returnTypes;
        $this->pathParameters    = $pathParameters;
        $this->queryParameters   = $queryParameters;
        $this->requestParameters = $requestParameters;
    }

    public function getReflectionMethod(): ReflectionMethod
    {
        return $this->reflectionMethod;
    }

    public function getGenerateClient(): GenerateClient
    {
        return $this->generateClient;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string[]
     */
    public function getHttpMethods(): array
    {
        return $this->httpMethods;
    }

    /**
     * @return ExportedType[]
     */
    public function getReturnTypes(): array
    {
        return $this->returnTypes;
    }

    /**
     * @return  ApiParameter[]
     */
    public function getPathParameters(): array
    {
        return $this->pathParameters;
    }

    public function hasPathParameters(): bool
    {
        return count($this->pathParameters) > 0;
    }

    /**
     * @return  ApiParameter[]
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    public function hasQueryParameters(): bool
    {
        return count($this->queryParameters) > 0;
    }

    /**
     * @return  ApiParameter[]
     */
    public function getRequestParameters(): array
    {
        return $this->requestParameters;
    }

    public function hasRequestParameters(): bool
    {
        return count($this->requestParameters) > 0;
    }

    /**
     * @return  ApiParameter[]
     */
    public function getAllParameters(): array
    {
        return [...$this->getPathParameters(), ...$this->getQueryParameters(), ...$this->getRequestParameters()];
    }

    public function getClientMethodName(): string
    {
        $name = $this->reflectionMethod->getName();
        if (str_ends_with($name, 'Action')) {
            $name = substr($name, 0, -6);
        }
        return $name;
    }
}
