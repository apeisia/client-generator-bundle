<?php

namespace Apeisia\ClientGeneratorBundle\Model;

use Apeisia\WatchBundle\Annotation\AttributeInstanceCreator;
use Illuminate\Support\Arr;
use Roave\BetterReflection\Reflection\ReflectionAttribute;
use Roave\BetterReflection\Reflection\ReflectionClass;

class ExportedProperty
{
    private string $name;
    private array $groups;
    private ?array $types;
    private ReflectionClass $implementingClass;

    /**
     * @var ReflectionAttribute[]
     */
    private array $attributes;

    public function __construct(string          $name,
                                array           $groups,
                                array           $types,
                                array           $attributes,
                                ReflectionClass $implementingClass)
    {
        $this->name              = $name;
        $this->groups            = $groups;
        $this->types             = $types;
        $this->attributes        = $attributes;
        $this->implementingClass = $implementingClass;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @return ExportedType[]
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getImplementingClass(): ReflectionClass
    {
        return $this->implementingClass;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @template T
     * @param class-string<T> $className
     * @return T[]|null
     */
    public function getAttribute(string $className): ?array
    {
        $found = [];
        foreach ($this->attributes as $attribute) {
            if ($attribute->getClass()->getName() === $className) {
                $found[] = $attribute;
            }
        }

        if (empty($found)) {
            return null;
        }

        return Arr::map($found, fn(ReflectionAttribute $attribute) => AttributeInstanceCreator::createInstance($this->implementingClass, $attribute));
    }

}
